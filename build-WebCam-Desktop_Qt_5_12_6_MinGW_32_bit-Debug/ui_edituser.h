/********************************************************************************
** Form generated from reading UI file 'edituser.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITUSER_H
#define UI_EDITUSER_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_edituser
{
public:
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;

    void setupUi(QDialog *edituser)
    {
        if (edituser->objectName().isEmpty())
            edituser->setObjectName(QString::fromUtf8("edituser"));
        edituser->resize(551, 383);
        label = new QLabel(edituser);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 20, 47, 13));
        label_2 = new QLabel(edituser);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 60, 47, 13));
        label_3 = new QLabel(edituser);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(20, 100, 71, 16));
        label_4 = new QLabel(edituser);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(20, 140, 47, 13));
        label_5 = new QLabel(edituser);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(30, 290, 131, 21));
        label_6 = new QLabel(edituser);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(20, 180, 47, 13));
        lineEdit = new QLineEdit(edituser);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(80, 20, 121, 21));
        lineEdit_2 = new QLineEdit(edituser);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(80, 60, 121, 21));
        lineEdit_3 = new QLineEdit(edituser);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(80, 100, 121, 21));
        lineEdit_4 = new QLineEdit(edituser);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(80, 140, 121, 21));
        lineEdit_5 = new QLineEdit(edituser);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(80, 190, 121, 21));
        pushButton = new QPushButton(edituser);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(20, 230, 75, 23));
        pushButton_2 = new QPushButton(edituser);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(120, 230, 75, 23));
        pushButton_3 = new QPushButton(edituser);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(220, 230, 75, 23));

        retranslateUi(edituser);

        QMetaObject::connectSlotsByName(edituser);
    } // setupUi

    void retranslateUi(QDialog *edituser)
    {
        edituser->setWindowTitle(QApplication::translate("edituser", "Dialog", nullptr));
        label->setText(QApplication::translate("edituser", "ID", nullptr));
        label_2->setText(QApplication::translate("edituser", "Name", nullptr));
        label_3->setText(QApplication::translate("edituser", "PassWord", nullptr));
        label_4->setText(QApplication::translate("edituser", "Class", nullptr));
        label_5->setText(QString());
        label_6->setText(QApplication::translate("edituser", "Age", nullptr));
        pushButton->setText(QApplication::translate("edituser", "Save", nullptr));
        pushButton_2->setText(QApplication::translate("edituser", "Update", nullptr));
        pushButton_3->setText(QApplication::translate("edituser", "Delete", nullptr));
    } // retranslateUi

};

namespace Ui {
    class edituser: public Ui_edituser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITUSER_H
