/********************************************************************************
** Form generated from reading UI file 'openwebcam.ui'
**
** Created by: Qt User Interface Compiler version 5.12.6
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENWEBCAM_H
#define UI_OPENWEBCAM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenWebcam
{
public:
    QPushButton *open;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QTableView *tableView;
    QPushButton *pushButton_load_tbl;
    QPushButton *pushButton;

    void setupUi(QDialog *OpenWebcam)
    {
        if (OpenWebcam->objectName().isEmpty())
            OpenWebcam->setObjectName(QString::fromUtf8("OpenWebcam"));
        OpenWebcam->resize(851, 422);
        open = new QPushButton(OpenWebcam);
        open->setObjectName(QString::fromUtf8("open"));
        open->setGeometry(QRect(9, 9, 75, 24));
        open->setStyleSheet(QString::fromUtf8("font: 10pt \"MS Shell Dlg 2\";"));
        scrollArea = new QScrollArea(OpenWebcam);
        scrollArea->setObjectName(QString::fromUtf8("scrollArea"));
        scrollArea->setGeometry(QRect(9, 39, 481, 281));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 479, 279));
        scrollArea->setWidget(scrollAreaWidgetContents);
        tableView = new QTableView(OpenWebcam);
        tableView->setObjectName(QString::fromUtf8("tableView"));
        tableView->setGeometry(QRect(520, 50, 321, 192));
        pushButton_load_tbl = new QPushButton(OpenWebcam);
        pushButton_load_tbl->setObjectName(QString::fromUtf8("pushButton_load_tbl"));
        pushButton_load_tbl->setGeometry(QRect(630, 270, 91, 31));
        pushButton = new QPushButton(OpenWebcam);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(364, 10, 111, 23));

        retranslateUi(OpenWebcam);

        QMetaObject::connectSlotsByName(OpenWebcam);
    } // setupUi

    void retranslateUi(QDialog *OpenWebcam)
    {
        OpenWebcam->setWindowTitle(QApplication::translate("OpenWebcam", "Dialog", nullptr));
        open->setText(QApplication::translate("OpenWebcam", "Options", nullptr));
        pushButton_load_tbl->setText(QApplication::translate("OpenWebcam", "Load User", nullptr));
        pushButton->setText(QApplication::translate("OpenWebcam", "Open Webrowser", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OpenWebcam: public Ui_OpenWebcam {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENWEBCAM_H
