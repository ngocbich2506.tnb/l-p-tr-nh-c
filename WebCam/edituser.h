#ifndef EDITUSER_H
#define EDITUSER_H
#include "login.h"
#include <QDialog>
#include <QWidget>
#include <QtSql>
#include <QSqlDatabase>
#include <QtDebug>
#include <QFileInfo>
namespace Ui {
class edituser;
}

class edituser : public QDialog
{
    Q_OBJECT

public:
    QSqlDatabase mydb;
    void connClose()
    {
        mydb.close();
        mydb.removeDatabase(QSqlDatabase::defaultConnection);

    }
    bool connOpen()
    {
        mydb = QSqlDatabase::addDatabase("QSQLITE");
        mydb.setDatabaseName("C:/sqlite/login.db");

        if(!mydb.open())
        {
            qDebug()<<("Failed to open DB");
            return false;
        }
        else
        {
            qDebug()<<("Connected...");
            return true;
        }
    }
    explicit edituser(QWidget *parent = nullptr);
    ~edituser();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::edituser *ui;
};

#endif // EDITUSER_H
