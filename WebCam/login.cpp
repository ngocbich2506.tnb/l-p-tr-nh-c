#include "login.h"
#include "ui_login.h"
#include "QMessageBox"
Login::Login(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Login)
{
    ui->setupUi(this);

    if(!connOpen())
        ui->label->setText("Failed to open DB");

    else
        ui->label->setText("Connected...");
}

Login::~Login()
{
    delete ui;
}


void Login::on_pushButton_clicked()
{
    QString username, password;

    username=ui->lineEdit_username->text();
    password=ui->lineEdit_Password->text();
    if(!connOpen()){
        qDebug()<<"Failed to open the databse";
        return;
    }
    connOpen();
    QSqlQuery qry;
    qry.prepare("select * from login Where name='"+username+"' and password='"+password+"'");

    if(qry.exec()){
        int count=0;
        while(qry.next())
        {
            count++;
        }
        if(count == 1){
            ui->label->setText("username and password are correct!");
            QMessageBox::information(this, "Login", "Username and password are correct");
            connClose();
            this->hide();
            OpenWebcam openwebcam;
            openwebcam.setModal(true);
            openwebcam.exec();
        }
        if(count > 1){
            ui->label->setText("Duplicate username and password!");
            QMessageBox::information(this, "Login", "Username and password are duplicate");
        }
        if(count < 1){
            ui->label->setText("username and password are not correct!");
            QMessageBox::warning(this, "Login", "Username and password are not correct");
        }
    }

}
