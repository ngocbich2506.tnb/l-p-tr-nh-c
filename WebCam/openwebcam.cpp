#include "openwebcam.h"
#include "ui_openwebcam.h"
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraImageCapture>
#include <QVBoxLayout>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QDesktopServices>
#include <QUrl>
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
OpenWebcam::OpenWebcam(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenWebcam)
{
    ui->setupUi(this);
//    QMediaPlayer* player =  new QMediaPlayer;
//    QVideoWidget* vw = new QVideoWidget;
//    player->setVideoOutput(vw);
//    player->setMedia()


    mCamera = new QCamera(this);
    mCameraViewfinder = new QCameraViewfinder(this);
    mCameraImageCapture = new QCameraImageCapture(mCamera, this);
    mLayout = new QVBoxLayout;
    mOpenMenu = new QMenu("Options", this);
    mEncenderAction = new QAction("Start", this);
    mApagarAction = new QAction("Stop", this);
   mCapturarAction = new QAction("Take a picture", this);

  mOpenMenu->addActions({mEncenderAction,mApagarAction, mCapturarAction});
    ui->open->setMenu(mOpenMenu);
    mCamera->setViewfinder(mCameraViewfinder);
    mLayout->addWidget(mCameraViewfinder);
    mLayout->setMargin(0);
    ui->scrollArea->setLayout(mLayout);

    connect(mEncenderAction, &QAction::triggered, [&](){
        mCamera->start();
    });
    connect(mApagarAction, &QAction::triggered, [&](){
        mCamera->stop();
    });
    connect(mCapturarAction, &QAction::triggered, [&](){
        auto filename = QFileDialog::getSaveFileName(this, "Take a picture", "", "JPEG(*jpg;*.jpeg);;PNG(*.png)");
        if (filename.isEmpty()){
            return;
        }
//        mCameraImageCapture->setCaptureDestination(
//                    QCameraImageCapture::CaptureToFile);
 QImageEncoderSettings imageEncoderSettings;
    imageEncoderSettings.setCodec("image/jpeg");
    imageEncoderSettings.setResolution(1600, 1200);

//        mCameraImageCapture->setEncodingSettings(imageEncoderSettings);
//        mCamera->setCaptureMode(QCamera::CaptureStillImage);
//
        mCamera->start();
        mCamera->searchAndLock();
        mCameraImageCapture->capture(filename);
     //   QString imagePath = QFileDialog::getSaveFileName(this,tr("save file"),"",tr("JPEG(*.jpg *.jpeg);;PNG(*.png)"));
        //mCameraImageCapture->imageSaved(4,imagePath);
        mCamera->unlock();
    });

}



OpenWebcam::~OpenWebcam()
{
    delete ui;
}

void OpenWebcam::on_pushButton_clicked()
{
    edituser edit;
    edit.setModal(true);
    edit.exec();

}

void OpenWebcam::on_pushButton_load_tbl_clicked()
{
        Login conn;
        QSqlQueryModel * modal = new QSqlQueryModel();

        conn.connOpen();
        QSqlQuery* qry =new QSqlQuery(conn.mydb);

        qry->prepare("select * from login");
        qry->exec();
        modal->setQuery(*qry);
        ui->tableView->setModel(modal);

        conn.connClose();
        qDebug() << (modal->rowCount());
}
