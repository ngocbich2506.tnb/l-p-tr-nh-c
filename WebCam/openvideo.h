#ifndef OPENVIDEO_H
#define OPENVIDEO_H
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QFileDialog>
#include <QProgressBar>
#include <QSlider>
#include <QDialog>
#include <QVBoxLayout>
namespace Ui {
class OpenVideo;
}

class OpenVideo : public QDialog
{
    Q_OBJECT

public:
    explicit OpenVideo(QWidget *parent = 0);
    ~OpenVideo();

private slots:
    void on_pushButton_open_clicked();

    void on_pushButton_play_clicked();

    void on_pushButton_start_clicked();

    void on_pushButton_pause_clicked();

private:
    Ui::OpenVideo *ui;

    QMediaPlayer* player;
    QVideoWidget* vw;
    QProgressBar* bar;
    QSlider* slider;

    QVBoxLayout *mainLayout;
    QWidget *mainWidget;
    QWidget *centralWidget;
};

#endif // OPENVIDEO_H
