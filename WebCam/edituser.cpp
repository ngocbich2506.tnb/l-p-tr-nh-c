#include "edituser.h"
#include "ui_edituser.h"
#include "QMessageBox"
edituser::edituser(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::edituser)
{
    ui->setupUi(this);
    Login conn;
    if(!conn.connOpen())

        ui->label_5->setText("Failed to open DB");
    else
        ui->label_5->setText("Connected...");
}

edituser::~edituser()
{
    delete ui;
}

void edituser::on_pushButton_clicked()
{
    Login conn;
    QString id, name, password, class_a, age;

    id=ui->lineEdit->text();
    name=ui->lineEdit_2->text();
    password=ui->lineEdit_3->text();
    age=ui->lineEdit_4->text();
    class_a=ui->lineEdit_5->text();

    if(!conn.connOpen()){
        qDebug()<<"Failed to open the databse";
        return;
    }
    connOpen();
    QSqlQuery qry;
    qry.prepare("insert into login (id, name, password, age, class_a) values ('"+id+"', '"+name+"', '"+password+"', '"+age+"', '"+class_a+"' )");

    if(qry.exec()){
        QMessageBox::information(this, tr("Save"), tr("Saved"));
        conn.close();
    }
    else
    {
        QMessageBox::critical(this, tr("error: "), qry.lastError().text());
    }
}

void edituser::on_pushButton_2_clicked()
{
    Login conn;
    QString id, name, password, class_a, age;

    id=ui->lineEdit->text();
    name=ui->lineEdit_2->text();
    password=ui->lineEdit_3->text();
    age=ui->lineEdit_4->text();
    class_a=ui->lineEdit_5->text();

    if(!conn.connOpen()){
        qDebug()<<"Failed to open the databse";
        return;
    }
    connOpen();
    QSqlQuery qry;
    qry.prepare("update login set id = '"+id+"', name = '"+name+"', password = '"+password+"', age = '"+age+"', class_a = '"+class_a+"' where id = '"+id+"'");

    if(qry.exec()){
        QMessageBox::information(this, tr("Edit"), tr("Updated"));
        conn.close();
    }
    else
    {
        QMessageBox::critical(this, tr("error: "), qry.lastError().text());
    }
}

void edituser::on_pushButton_3_clicked()
{
    Login conn;
    QString id, name, password, class_a, age;

    id=ui->lineEdit->text();
    name=ui->lineEdit_2->text();
    password=ui->lineEdit_3->text();
    age=ui->lineEdit_4->text();
    class_a=ui->lineEdit_5->text();

    if(!conn.connOpen()){
        qDebug()<<"Failed to open the databse";
        return;
    }
    connOpen();
    QSqlQuery qry;
    qry.prepare("Delete from login  where id = '"+id+"'");

    if(qry.exec()){
        QMessageBox::information(this, tr("Delete"), tr("Deleted"));
        conn.close();
    }
    else
    {
        QMessageBox::critical(this, tr("error: "), qry.lastError().text());
    }
}
