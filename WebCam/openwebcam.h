#ifndef OPENWEBCAM_H
#define OPENWEBCAM_H
#include <QSqlDatabase>
#include <QDialog>
#include "login.h"
#include <QMediaPlayer>
#include <QVideoWidget>
#include <QFileDialog>
#include <QProgressBar>
#include <QSlider>
#include "openvideo.h"
namespace Ui {
class OpenWebcam;
}
class QCamera;
class QCameraViewfinder;
class QCameraImageCapture;
class QVBoxLayout;
class QMenu;
class QAction;

class OpenWebcam : public QDialog
{
    Q_OBJECT
    QWidget *centralWidget;
public:

    explicit OpenWebcam(QWidget *parent = nullptr);
    ~OpenWebcam();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_load_tbl_clicked();

    void on_pushButton_2_clicked();
    void on_pushButton_2_pressed();

    void on_open_clicked();

private:
    Ui::OpenWebcam *ui;
    QCamera *mCamera;
    QCameraViewfinder *mCameraViewfinder;
    QCameraImageCapture *mCameraImageCapture;
    QVBoxLayout *mLayout;
    QMenu *mOpenMenu;
    QAction *mEncenderAction;
    QAction *mApagarAction;
    QAction *mCapturarAction;
    QSqlDatabase mydb;

    QMediaPlayer* player;
    QVideoWidget* vw;
    QProgressBar* bar;
    QSlider* slider;

    OpenVideo *openvideo;
};

#endif // OPENWEBCAM_H
