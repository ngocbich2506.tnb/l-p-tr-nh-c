#include "openvideo.h"
#include "ui_openvideo.h"

OpenVideo::OpenVideo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OpenVideo)
{

    ui->setupUi(this);

    player = new QMediaPlayer(this);
    vw = new QVideoWidget(this);
    player->setVideoOutput(vw);
//    ui->widget->setLayout(vw);
//    ui->setupUi(this);
//    centralWidget = new QWidget(vw);
//    this->setCentralWidget(vw);
//    QTLog(<<centralWidget());
}

OpenVideo::~OpenVideo()
{
    delete ui;
}

void OpenVideo::on_pushButton_open_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this, "Open a file", "", "Video File(*.*)");
    on_pushButton_open_clicked();
    player->setMedia(QUrl::fromLocalFile(filename));

}

void OpenVideo::on_pushButton_play_clicked()
{
    player->play();
}

void OpenVideo::on_pushButton_start_clicked()
{
    player->pause();
}

void OpenVideo::on_pushButton_pause_clicked()
{
    player->stop();
}
