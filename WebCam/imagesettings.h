#ifndef IMAGESETTINGS_H
#define IMAGESETTINGS_H

#include <QDialog>

namespace Ui {
class imagesettings;
}

class imagesettings : public QDialog
{
    Q_OBJECT

public:
    explicit imagesettings(QWidget *parent = nullptr);
    ~imagesettings();

private:
    Ui::imagesettings *ui;
};

#endif // IMAGESETTINGS_H
